export const Config = {
  ThinHeading: "WHAT PEOPLE SAY?",
  BoldHeading: "Testimonials.",
  LowOpacityHeading: "Testimonials",
  Testimonials: [
    {
      para: "Cloverlemon created a professional NFT Launch Platform for our NFTs, discord channel and twitter. Literally a turnkey solution for a successful NFT Project and the results from the launch day affirmed that Cloverlemon solutions work.",
      img: "/assets/home_testimonials/1.png",
      name: " Master Fon",
      designation: "Founder of MandalaFON",
    },    
  ],
};
